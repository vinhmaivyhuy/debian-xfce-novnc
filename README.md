# Docker running Debian linux with xfce / access with novnc

Running Debian / xfce accessed with novnc (web based)

## Getting started

- [ ] Install [Docker for Desktop](https://docs.docker.com/get-docker/) on your local pC or install Docker on a server
- [ ] Edit authorized_keys: add your ssh public key (~/.ssh/id_rsa.pub) at the end of the file
- [ ] Edit config if required (default setting for geometry is 1920x1080)
- [ ] Use PowerShell or a wsl terminal to run:

```
./build.sh
```
 
 or

```
docker build --tag debianxfcenovnc:latest --label debianxfcenovnc .
```

- [ ] then once the image has been build, update the ports on file run-docker.sh, then run

```
./run-docker.sh
```
 
 or

```
docker run -d -p 5901:5901 -p 6080:6080 -p 222:22 debianxfcenovnc:latest 
```


## Access the container running on your local PC:

- [ ] Access with a local browser: http://localhost:6080

## Access the container running on an external server through a ssh tunnel:
- [ ] Use PowerShell or a wsl terminal to run:
```
ssh -L 6080:localhost:6080 'server_running_the_container'

```
- [ ] Access with a local browser: http://localhost:6080

